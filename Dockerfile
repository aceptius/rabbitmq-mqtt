FROM rabbitmq:3.7-management

LABEL maintainer="mkelcik@gmail.com"

COPY plugins/* /plugins

RUN rabbitmq-plugins enable --offline rabbitmq_mqtt
RUN rabbitmq-plugins enable --offline rabbitmq_message_timestamp

RUN echo 'NODENAME=rabbit@localhost' > /etc/rabbitmq/rabbitmq-env.conf
